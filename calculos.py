import pandas as pd

def read_file(path,sheet):
    df=pd.read_excel(path,sheet_name=sheet)
    df_nodos=pd.DataFrame(columns=['nodo_1','nodo_2'])
    df_nodos[['nodo_1','nodo_2']]=df[['nodo_1','nodo_2']]
    df_nodos=df_nodos.drop_duplicates(subset=['nodo_1','nodo_2'])
    nodo_1=df_nodos['nodo_1'].to_list()
    nodo_2=df_nodos['nodo_2'].to_list()
    df=df.groupby(['nodo_1','nodo_2'])
    return  df, nodo_1,nodo_2

def main():
    path='~/proyecto_transmilenio/data_input/PSO20220223.xls'
    sheet='Arcos Comerciales y T.Recorrido'
    path_out='~/proyecto_transmilenio/data_output/resultado.csv'
    df,nodo_1,nodo_2=read_file(path,sheet)
    df_t=pd.DataFrame(columns=['linea','sentido','tipodia','sublinea','tr_optimo','tr_minimo','tr_maximo','nodo','nodo_2'])
    df_t_values=pd.DataFrame(columns=['linea','sentido','tipodia','sublinea','tr_optimo','tr_minimo','tr_maximo','nodo','nodo_2'])
    for i in range(len(nodo_1)):
        df_group=df.get_group((nodo_1[i],nodo_2[i]))
        df_group=df_group.reset_index()
        df_t_values[['linea','sentido','tipodia','sublinea']]=df_group.iloc[[0],[1,13,6,12]]
        df_t_values['tr_optimo']=df_group['tr_minimo'].min()
        df_t_values['tr_minimo']=df_group['tr_minimo'].min()
        df_t_values['tr_maximo']=df_group['tr_maximo'].max()
        df_t_values['nodo']=df_group.iloc[[0],[2]]
        df_t_values['nodo_2']=df_group.iloc[[0],[3]]
        df_t=df_t.append(df_t_values)
    df_t.to_csv(path_out,index=False)

if __name__ == '__main__':
    main()




